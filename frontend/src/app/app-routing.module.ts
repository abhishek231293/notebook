import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './auth/login/login.component';
import {SignupComponent} from './auth/signup/signup.component';
import {PageNotFoundComponent} from './error/page-not-found/page-not-found.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FollowComponent} from './follow/follow.component';
import {MyVideosComponent} from './my-videos/my-videos.component';
import {VideoComponent} from "./video/video.component";


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'sign-up', component: SignupComponent },
  { path: 'news-feeds', component: DashboardComponent },
  { path: 'follow', component: FollowComponent },
  { path: 'my-videos', component: MyVideosComponent },
  { path: 'video/:videoId', component: VideoComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
