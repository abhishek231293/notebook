import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noteTime'
})
export class NoteTimePipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    let minutes = Math.floor(value / 60);
    let seconds = value - minutes * 60;
    let hours = Math.floor(value / 3600);

    return ((hours?(hours + ':'):'') + ((minutes<9)?('0'+minutes):minutes) + ':' + ((seconds<9)?('0'+seconds):seconds));
  }

}
