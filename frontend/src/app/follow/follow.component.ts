import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import { CommonService , AuthenticationService } from '../_services';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.css']
})
export class FollowComponent implements OnInit {

  apiRoute:string;
  searchData = null;
  searchFields = { 'text': null};
  loading = false;
  unfollowedList = [];

  constructor(private _http: HttpClient,
              private router: Router,
              private authenticationService: AuthenticationService,
              private _commonService: CommonService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    const currentUser = JSON.parse(localStorage.getItem('eNotebook'));

    if (currentUser) {
      this.getUserList();
    } else {
      this.router.navigate(['/login']);
    }
  }

  getUserList() {

    if(this.searchData){
      this.apiRoute  = 'get-user-list-by-name';
    }else{
      this.apiRoute  = 'get-user-list';
    }

    let data = null;
    this.searchFields = { 'text': this.searchData};

    data = {
      search: this.searchFields
    };

    this.loading = true;
    this.spinner.show();
    this._commonService.postRequestCreator(data, this.apiRoute).subscribe((response: any) => {
      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
      }else if (response.status == 200){
        this.unfollowedList = [];
        this.unfollowedList = response.result;
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }
      this.spinner.hide();
    });
  }

  followUser(userId){
    this.apiRoute  = 'follow-user';
    let data = null;

    data = {
      user_id: userId
    };

    this.loading = true;
    this.spinner.show();
    this._commonService.postRequestCreator(data, this.apiRoute).subscribe((response: any) => {
      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
      }else if (response.status == 200){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
        this.searchData = null;
        this.getUserList();
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }
      this.spinner.hide();
    });
  }

  unFollowUser(followid){
    this.apiRoute  = 'unfollow-user';
    let data = null;

    data = {
      user_id: followid
    };

    this.loading = true;
    this.spinner.show();
    this._commonService.postRequestCreator(data, this.apiRoute).subscribe((response: any) => {
      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
      }else if (response.status == 200){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
        this.searchData = null;
        this.getUserList();
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }
      this.spinner.hide();
    });
  }

}
