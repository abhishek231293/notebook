import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService , AuthenticationService } from '../../_services';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  submitted = false;
  loading = false;
  apiRoutes = '';
  error = '';
  namePattern = '^[a-zA-Z ]*$';
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private _commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      'name': new FormControl('', [Validators.required, Validators.pattern(this.namePattern)]),
      'email': new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]),
      'password': new FormControl('', [Validators.required, Validators.minLength(8)]),
      'confirmPassword': new FormControl('', [Validators.required])
    }, {validator: this.checkPasswords });
  }

  // convenience getter for easy access to form fields
  get f() { return this.signupForm.controls; }
  get passwordConfirm() { return this.signupForm.errors; }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value;

    return pass === confirmPass ? null : { notSame: true }
  }

  signUp() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.signupForm.invalid) {
      return;
    }

    this.apiRoutes  = 'sign-up';

    this.loading = true;
    this._commonService.postRequestCreator(this.signupForm.value, this.apiRoutes).subscribe((response: any) => {

      if(response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
      }else if(response.status == 200){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigate(['login']);
      }else{
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Something went wrong',
          showConfirmButton: false,
          timer: 1500
        });
      }

      this.loading = false;
    });
  }

}
