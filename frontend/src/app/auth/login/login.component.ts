import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService , AuthenticationService } from '../../_services';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  returnUrl: string;
  submitted = false;
  loading = false;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private _commonService: CommonService
  ) { }

  ngOnInit() {
    const currentUser = JSON.parse(localStorage.getItem('eNotebook'));

    if (currentUser) {
      this.router.navigate(['/news-feeds']);
    } else {
      this.loginForm = this.formBuilder.group({
        'email': new FormControl('', [Validators.required]),
        'password': new FormControl('', [Validators.required])
      });

      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/news-feeds';

    }
  }

  get f() { return this.loginForm.controls; }

  signIn() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f)
      .subscribe(
        data => {
          if(data.length != 0){
            this.router.navigate(['/news-feeds']);
          }else{
            this.loading = false;
            this.error = 'Email or Password Doesn\'t Exist!';
          }
        },
        error => this.handleError(error)
      );
  }

  handleError(errorData){
    this.error = errorData;
    this.loading = false;
  }

}
