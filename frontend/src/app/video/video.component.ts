import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CommonService , AuthenticationService } from '../_services';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import {  TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
declare var require: any

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  addNoteForm: FormGroup;
  modalRef: BsModalRef;
  loading = false;
  invalidVideo = false;
  submitted = false;
  apiRoutes = '';
  videoId = null;
  noteData = null;
  videoStopTime = 0;
  public video: any;
  public player: any;
  constructor(private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService,
              private _commonService: CommonService,
              private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    const currentUser = JSON.parse(localStorage.getItem('eNotebook'));

    if (currentUser) {
      this.route.paramMap.subscribe( (params: ParamMap) => {
        this.videoId = params.get('videoId');
      });
      this.getVideo();
    } else {
      this.router.navigate(['/login']);
    }
  }

  getVideo(){
    this.submitted = true;
    this.invalidVideo = false;
    this.apiRoutes  = 'get-video';
    this.loading = true;
    this.spinner.show();

    const data = {'video_id' : this.videoId};
    this._commonService.postRequestCreator(data, this.apiRoutes).subscribe((response: any) => {

      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });

      }else if (response.status == 200){

        if(response.result){

          const videoData = response.result.video;
          this.noteData = response.result.note;

          if(videoData){
            const links = videoData.link.split('/');

            if(links){
                const youtubeId = links[links.length-1];
                const idset = youtubeId.split('?');

                if(idset){
                  const youtubeId = idset[0];
                  const YTPlayer = require('yt-player');
                  this.player = new YTPlayer('#player');
                  this.player.load(youtubeId);
                  this.player.play();
                  this.spinner.hide();
                  return;
                }
            }
          }
        }

        this.invalidVideo = true;

      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }

      this.spinner.hide();
    });
  }

  get n() { return this.addNoteForm.controls; }

  getNote(){
    this.submitted = true;
    this.apiRoutes  = 'get-note';
    this.loading = true;
    this.spinner.show();

    const data = {'video_id' : this.videoId};
    this._commonService.postRequestCreator(data, this.apiRoutes).subscribe((response: any) => {

      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });

      }else if (response.status == 200){
        this.noteData = response.result;
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }

      this.spinner.hide();
    });
  }

  addNewNote() {

    this.submitted = true;
    if (this.addNoteForm.invalid) {
      return;
    }

    this.apiRoutes  = 'add-note';
    this.loading = true;
    this.spinner.show();

    const data = {'note' : this.addNoteForm.value.note, 'video_id' : this.videoId, 'video_stop_time' : this.videoStopTime};
    this._commonService.postRequestCreator(data, this.apiRoutes).subscribe((response: any) => {

      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });

      }else if (response.status == 200){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });

        this.getNote();
        this.addNoteForm.get('note').setValue('');
        this.submitted = false;
        this.modalRef.hide();
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.modalRef.hide();
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }

      this.spinner.hide();
    });
  }

  openNoteModal(template: TemplateRef<any>) {
    this.player.pause();
    this.videoStopTime = this.player.getCurrentTime();
    this.submitted = false;
    this.addNoteForm = this.formBuilder.group({
      'note': new FormControl('', [Validators.required])
    });
    this.modalRef = this.modalService.show(template);
  }
}
