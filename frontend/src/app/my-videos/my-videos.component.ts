import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService , AuthenticationService } from '../_services';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import {  TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-my-videos',
  templateUrl: './my-videos.component.html',
  styleUrls: ['./my-videos.component.css']
})
export class MyVideosComponent implements OnInit {

  addVideoForm: FormGroup;
  addNoteForm: FormGroup;
  modalRef: BsModalRef;
  loading = false;
  userId = null;
  player;
  submitted = false;
  apiRoutes = '';
  savedVideoList = [];
  youtubePattern = '^(?:https?:\\/\\/)?(?:m\\.|www\\.)?(?:youtu\\.be\\/|youtube\\.com\\/(?:embed\\/|v\\/|watch\\?v=|watch\\?.+&v=))((\\w|-){11})(\\?\\S*)?$';
  // youtubePattern = '^(https|http):\\/\\/(?:www\\.)?youtube.com\\/embed\\/[-a-zA-Z0-9_]+';
  constructor(private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService,
              private _commonService: CommonService,
              private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {

    const currentUser = JSON.parse(localStorage.getItem('eNotebook'));

    if (currentUser) {
      this.userId = currentUser['user']['id'];
      this.getSavedVideo();
    } else {
      this.router.navigate(['/login']);
    }

  }

  changeVideoType(video_id, type){

    this.apiRoutes  = 'change-video-type';
    let data = {'video_id':video_id, 'type':type};

    this.loading = true;
    this.spinner.show();
    this._commonService.postRequestCreator(data, this.apiRoutes).subscribe((response: any) => {
      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
      }else if (response.status == 200){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
        this.getSavedVideo();
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }
      this.spinner.hide();
    });

  }

  getSavedVideo(){
    this.apiRoutes  = 'get-saved-video';
    let data = {};

    this.loading = true;
    this.spinner.show();
    this._commonService.postRequestCreator(data, this.apiRoutes).subscribe((response: any) => {
      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
      }else if (response.status == 200){
        this.savedVideoList = [];
        this.savedVideoList = response.result;
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }
      this.spinner.hide();
    });
  }

  addNewVideo(){

    this.submitted = true;

    if (this.addVideoForm.invalid) {
      return;
    }

    this.apiRoutes  = 'add-new-video';
    this.loading = true;
    // this.spinner.show();
    this._commonService.postRequestCreator(this.addVideoForm.value, this.apiRoutes).subscribe((response: any) => {

      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });

      }else if (response.status == 200){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });

        this.getSavedVideo();

        this.addVideoForm.get('link').setValue('');
        this.addVideoForm.get('type').setValue('private');
        this.submitted = false;
        this.modalRef.hide();
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.modalRef.hide();
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }

      this.spinner.hide();
    });
  }

  openModal(template: TemplateRef<any>) {
    this.submitted = false;
    this.addVideoForm = this.formBuilder.group({
      'link': new FormControl('', [Validators.required, Validators.pattern(this.youtubePattern)]),
      'type': new FormControl('private', [Validators.required]),
      'title': new FormControl('', [Validators.required]),
      'description': new FormControl('', [Validators.required])
    });
    this.modalRef = this.modalService.show(template);
  }

  get f() { return this.addVideoForm.controls; }
  get n() { return this.addNoteForm.controls; }

}
