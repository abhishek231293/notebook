import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService , AuthenticationService } from '../_services';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  loading = false;
  apiRoutes = '';
  newsFeedsVideoList = [];
  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService,
              private _commonService: CommonService,
              private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    const currentUser = JSON.parse(localStorage.getItem('eNotebook'));

    if (currentUser) {
      this.getNewsFeedsVideo();
    } else {
      this.router.navigate(['/login']);
    }
  }

  getNewsFeedsVideo(){
    this.apiRoutes  = 'get-news-feeds-video';
    let data = {};

    this.loading = true;
    this.spinner.show();
    this._commonService.postRequestCreator(data, this.apiRoutes).subscribe((response: any) => {
      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
      }else if (response.status == 200){
        this.newsFeedsVideoList = [];
        this.newsFeedsVideoList = response.result;
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }
      this.spinner.hide();
    });
  }

  saveVideo(videoId) {
    this.apiRoutes  = 'save-video';
    let data = { 'video_id' : videoId };

    this.loading = true;
    this.spinner.show();
    this._commonService.postRequestCreator(data, this.apiRoutes).subscribe((response: any) => {
      this.loading = false;
      if (response.status == 503){
        Swal.fire({
          position: 'top-end',
          icon: 'warning',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
      }else if (response.status == 200){
        this.getNewsFeedsVideo();
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: response.message,
          showConfirmButton: false,
          timer: 1500
        });
      }else if (response.status == 401){
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Session timeout login again',
          showConfirmButton: false,
          timer: 1500
        });
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }else{
        this.authenticationService.logout();
        this.router.navigate(['login']);
      }
      this.spinner.hide();
    });
  }

}
