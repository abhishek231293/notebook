import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { AuthenticationService } from '../_services';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
    private currentUser;
    private API_URL = environment.apiUrl;

  constructor(private _http: HttpClient,private router: Router) {}

    postRequestCreator(param, route): Observable<{}> {
        return this._http.post<{}>(this.API_URL + '/' + route, param);
    }

    getRequestCreator(route): Observable<{}> {
      return this._http.get<{}>(this.API_URL + '/' + route);
  }
}
