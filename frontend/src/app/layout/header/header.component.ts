import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonService } from '../../_services/common.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../_services'
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  authUser;
  username;
  baseUrl = environment.baseUrl;
  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private formBuilder: FormBuilder,
              private _commonService: CommonService) { }

  ngOnInit(): void {
    const currentUser = JSON.parse(localStorage.getItem('eNotebook'));

    if (currentUser) {
      this.authUser = currentUser['user'];
      this.username = this.authUser['name'];
    } else {
      this.router.navigate(['/login']);
    }

  }


  logout() {
    this.authenticationService.logout();
  }
}
