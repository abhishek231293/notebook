export const environment = {
  production: true,
  apiUrl: 'http://notebook.zerodesk.in/api/api',
  baseUrl: 'http://notebook.zerodesk.in',
};
