<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// '/var/www/bambooapp/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'

Route::group([
    'middleware' => ['cors', 'api'],
], function ($router) {

    Route::post('sign-up', 'UserController@signUp');
    Route::post('sign-in', 'UserController@signIn');
    Route::post('get-user-list', 'UserController@getUserList');
    Route::post('get-user-list-by-name', 'UserController@getUserListByName');
    Route::post('follow-user', 'UserController@followUser');
    Route::post('unfollow-user', 'UserController@unfollowUser');

    Route::post('add-new-video', 'VideoController@addNewVideo');
    Route::post('get-saved-video', 'VideoController@getSavedVideo');
    Route::post('get-news-feeds-video', 'VideoController@getNewsFeedsVideo');
    Route::post('save-video', 'VideoController@saveVideo');
    Route::post('add-note', 'VideoController@addNote');
    Route::post('get-video', 'VideoController@getVideoById');
    Route::post('get-note', 'VideoController@getNote');
    Route::post('change-video-type', 'VideoController@changeVideoType');

});
