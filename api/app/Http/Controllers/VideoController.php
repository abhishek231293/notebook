<?php

namespace App\Http\Controllers;

use App\FollowedVideo;
use App\Follower;
use App\Video;
use App\VideoNote;
use Illuminate\Http\Request;

class VideoController extends Controller
{

    private $user = null;

    function __construct()
    {
        $this->user = auth('api')->user();
    }

    protected function addNewVideo(Request $request){

        try {
            if($this->user){
                $data = $request->all();

//                $embedLink = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","https://www.youtube.com/embed/$1", $data['link']);


                $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
                $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';
                $youtube_id = '';

                if (preg_match($longUrlRegex, $data['link'], $matches)) {
                    $youtube_id = $matches[count($matches) - 1];
                }

                if (preg_match($shortUrlRegex, $data['link'], $matches)) {
                    $youtube_id = $matches[count($matches) - 1];
                }
                $embedLink = 'https://www.youtube.com/embed/' . $youtube_id ;


                $videoModel = new Video();
                $videoModel->title = $data['title'];
                $videoModel->description = $data['description'];
                $videoModel->link = $embedLink;
                $videoModel->type = $data['type'];
                $videoModel->user_id = $this->user->id;
                $videoModel->save();

                return $this->sendResponse(200, 'Video added Successfully', []);

            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }

    protected function addNote(Request $request){

        try {
            if($this->user){
                $data = $request->all();
                $noteModel = new VideoNote();
                $noteModel->video_id = $data['video_id'];
                $noteModel->user_id = $this->user->id;
                $noteModel->note = $data['note'];
                $noteModel->time = floor($data['video_stop_time']);
                $noteModel->save();

                return $this->sendResponse(200, 'Note added Successfully', []);

            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }

    protected function changeVideoType(Request $request){

        try {
            if($this->user){
                $data = $request->all();

                $videoModel = new Video();
                $videoModel->where('id', $data['video_id'])
                            ->update(['type'=>( ($data['type'] ==  'public') ? 'private' : 'public')]);

                return $this->sendResponse(200, 'Privacy Changed Successfully', []);

            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }

    protected function getNote(Request $request){
        try {
            if($this->user){

                $data = $request->all();

                $NoteModel = new VideoNote();
                $noteList = $NoteModel->where('user_id', $this->user->id)
                    ->where('video_id', $data['video_id'])
                    ->orderBy('time')
                    ->get()->toArray();
                return $this->sendResponse(200, 'Video Fetched Successfully', $noteList);


            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }
    }

    protected function getVideoById(Request $request){
        try {
            if($this->user){

                $data = $request->all();

                $videoModel = new Video();
                $query = $videoModel->leftJoin('followed_videos', 'followed_videos.video_id', 'videos.id');

                $query->orWhere(function ($query) use($data) {
                    $query->where('videos.user_id',$this->user->id)
                        ->where('videos.id', '=', $data['video_id']);
                });

                $query->orWhere(function ($query) use($data) {
                    $query->where('followed_videos.user_id',$this->user->id)
                        ->where('followed_videos.video_id', '=', $data['video_id'])
                        ->where('videos.type', '=', 'public');
                });
                $videoList = $query->first();

                $NoteModel = new VideoNote();
                $noteList = $NoteModel->where('user_id', $this->user->id)
                    ->where('video_id', $data['video_id'])
                    ->orderBy('time')
                    ->get()->toArray();

                $dataSet = array('video'=>$videoList, 'note'=>$noteList);
                return $this->sendResponse(200, 'Video Fetched Successfully', $dataSet);


            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }
    }
    protected function getSavedVideo(Request $request){

        try {
            if($this->user){

                $data = $request->all();
                $followedVideoModel = new FollowedVideo();
                $saveVideo = $followedVideoModel->where('user_id', $this->user->id)
                    ->pluck('video_id');

                $followerModel = new Follower();
                $followers = $followerModel->where('follower_id', $this->user->id)
                    ->pluck('following_id');

                $videoModel = new Video();

                if($saveVideo){
                    $query = $videoModel->where(function ($query) use($saveVideo) {
                        $query->whereIn('id',$saveVideo)
                        ->where('type', '=', 'public');
                    });
                   $query->orWhere('user_id',$this->user->id);

                }else{
                    $query = $videoModel->where('user_id',$this->user->id);
                }

                if(isset($data['video_id']) && $data['video_id']){
                    $query = $query->where(function ($query) use($data) {
                        $query->where('id',$data['video_id'])
                            ->where('type', '=', 'public');
                    });

                    $query = $query->orWhere(function ($query) use($data) {
                        $query->where('id',$data['video_id'])
                            ->where('user_id', '=', $this->user->id);
                    });

                    $videoList = $query->first();

                    $NoteModel = new VideoNote();
                    $noteList = $NoteModel->where('user_id', $this->user->id)
                                            ->where('video_id', $data['video_id'])
                                            ->orderBy('time')
                                            ->get()->toArray();

                    $dataSet = array('video'=>$videoList, 'note'=>$noteList);
                    return $this->sendResponse(200, 'Video Fetched Successfully', $dataSet);
                }else{
                    $videoList = $query->get()->toArray();
                    return $this->sendResponse(200, 'Video Fetched Successfully', $videoList);
                }


            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }

    protected function saveVideo(Request $request){

        try {
            if($this->user){

                $data = $request->all();
                $followVideoModel = new FollowedVideo();
                $followVideoModel->video_id = $data['video_id'];
                $followVideoModel->user_id = $this->user->id;
                $followVideoModel->save();

                return $this->sendResponse(200, 'Video Saved Successfully', []);

            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }

    protected function getNewsFeedsVideo(Request $request){

        try {
            if($this->user){

                $followerModel = new Follower();
                $followers = $followerModel->where('follower_id', $this->user->id)
                    ->pluck('following_id');

                $followedVideoModel = new FollowedVideo();
                $saveVideo = $followedVideoModel->where('user_id', $this->user->id)
                                                ->pluck('video_id');

                $videoModel = new Video();
                $videoList = array();

                if($followers){
                    $query = $videoModel->where(function ($query) use($followers) {
                        $query->whereIn('user_id', $followers)
                            ->where('type', '=', 'public');
                    });

                    if($saveVideo){
                        $query->whereNotIn('id',$saveVideo);
                    }

                    $videoList = $query->get()->toArray();
                }


                return $this->sendResponse(200, 'News Feeds Fetched Successfully', $videoList);

            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }

    private function sendResponse($status = 503, $message = 'Something went wrong. Please try again.', $data = []){
        $response = new \stdClass();
        $response->status = $status;
        $response->message = $message;
        $response->result = $data;
        return response()->json($response, 200);
    }
}
