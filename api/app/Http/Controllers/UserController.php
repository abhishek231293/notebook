<?php

namespace App\Http\Controllers;

use App\FollowedVideo;
use App\Follower;
use App\Http\Requests\SignupRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use JWTAuth;

class UserController extends Controller
{

    private $user = null;

    function __construct()
    {
        $this->user = auth('api')->user();
    }

    protected function signUp(SignupRequest $request){
        try{

            $data = $request->all();
            $userModel = new User();
            $userModel->name = $data['name'];
            $userModel->email = $data['email'];
            $userModel->password = Hash::make($data['password']);
            $userModel->save();

            if($userModel->id){
                $code = str_replace(' ', '',$data['name']).$userModel->id;
                $userModel->where('id',$userModel->id)->update(['user_code'=>strtolower($code)]);
            }
            $response = new \stdClass();
            $response->status = 200;
            $response->message = 'Sign-up Successfully';
            return response()->json($response, 200);
        }catch (\Exception $e){
            $response = new \stdClass();
            $response->status = 504;
            $response->message = $e->getMessage();
            return response()->json($response, 200);
        }
    }

    protected function signIn(Request $request){
        try{

            $credentials = request(['email', 'password']);

            // try to auth and get the token using api authentication
            if (!$token = auth('api')->attempt($credentials)) {
                // if the credentials are wrong we send an unauthorized error in json format
                return $this->sendResponse(401, 'Invalid User Id or Password');
            }
            $currentUser = auth('api')->user();

            $returnData = ['token'=>$token, 'user'=>$currentUser];

            return $this->sendResponse(200, 'Signed In Successfully', $returnData);

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }
    }

    protected function getUserList(Request $request){

        try{

            if($this->user){

                $data = $request->all();
                $followers = null;
                $followIds = null;

                $followerModel = new Follower();
                $followers = $followerModel->where('follower_id', $this->user->id)
                    ->pluck('following_id');


                $userModel = new User();
                $query = $userModel->leftJoin('followers', 'followers.following_id', 'users.id')
                                   ->where('users.id', '!=', $this->user->id);

                if($followers){
                    $query->whereIn('users.id', $followers);
                }

                $userList = $query->select('users.id', 'followers.id as follow_id', 'users.user_code','users.name','users.user_pic')
                    ->orderBy('users.name')
                    ->groupBy('users.id')->get()->toArray();

                return $this->sendResponse(200, 'User Fetched Successfully', $userList);

            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }
    protected function getUserListByName(Request $request){

        try{

            if($this->user){

                $data = $request->all();
                $followers = null;
                $followIds = null;

                $followerModel = new Follower();
                $followers = $followerModel->where('follower_id', $this->user->id)
                    ->pluck('following_id');


                $userModel = new User();
                $query = $userModel->leftJoin('followers', 'followers.following_id', 'users.id')
                    ->where('users.id', '!=', $this->user->id);

                if($followers){
                    $query->whereIn('users.id', $followers);
                }

                $query->where(function ($query) use($data) {
                    $query->where('users.name', 'like', '%'.$data['search']['text'].'%')
                        ->orWhere('users.user_code', 'like', '%'.$data['search']['text'].'%');
                });

                $followedUserList = $query->select('users.id', 'followers.id as follow_id', 'users.user_code','users.name','users.user_pic')
                                ->orderBy('users.name')
                                ->groupBy('users.id')->get()->toArray();

                $nonFollowerQuery = $userModel->where('users.id', '!=', $this->user->id);
                if($followers){
                    $nonFollowerQuery->whereNotIn('users.id', $followers);
                }

                $nonFollowerQuery->where(function ($query) use($data) {
                    $query->where('users.name', 'like', '%'.$data['search']['text'].'%')
                        ->orWhere('users.user_code', 'like', '%'.$data['search']['text'].'%');
                });
                $unfollowedUserList = $nonFollowerQuery->select('users.id',  'users.user_code','users.name','users.user_pic')
                    ->orderBy('users.name')
                    ->groupBy('users.id')->get()->toArray();

                $unfollowedList = array();
                array_walk($unfollowedUserList,function($responses,$index) use(&$unfollowedList){
                    $responses['follow_id'] = null;
                    $unfollowedList[] = $responses;
                });
                $userList = array_merge($followedUserList,$unfollowedList);

                return $this->sendResponse(200, 'User Fetched Successfully', $userList);

            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }

    // Comparison function
    function sort_array($element1, $element2) {
        return $element1 - $element2;
    }

    protected function followUser(Request $request){

        try{

            if($this->user){

                $data = $request->all();

                $followerModel = new Follower();
                $followerModel->follower_id = $this->user->id;
                $followerModel->following_id = $data['user_id'];
                $followerModel->save();

                return $this->sendResponse(200, 'User Followed Successfully', []);

            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }

    protected function unfollowUser(Request $request){

        try{

            if($this->user){

                $data = $request->all();
                $followerModel = new Follower();

                $followingDetail = $followerModel->leftJoin('videos', 'videos.user_id', 'followers.following_id')
                    ->where('followers.following_id',$data['user_id'])
                    ->pluck('videos.id');

                if($followingDetail){
                    $follwedVideoModel = new FollowedVideo();
                    $follwedVideoModel->whereIn('video_id',$followingDetail)
                                      ->where('user_id',$this->user->id)->delete();
                }
                $followerModel->where('following_id', $data['user_id'])
                              ->where('follower_id',$this->user->id)
                              ->delete();

                return $this->sendResponse(200, 'User Unfollowed Successfully', []);

            }else{
                return $this->sendResponse(401, 'Token Expired, Please login again!');
            }

        }catch (\Exception $e){
            return $this->sendResponse(504, $e->getMessage());
        }

    }

    private function  sendResponse($status = 503, $message = 'Something went wrong. Please try again.', $data = []){
        $response = new \stdClass();
        $response->status = $status;
        $response->message = $message;
        $response->result = $data;
        return response()->json($response, 200);
    }
}
